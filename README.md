CS - Assignment
===============

Our custom-build server logs different events to a file. Every event has 2 entries in a log - one entry when the event was started and another when
the event was finished. The entries in a log file have no specific order (it can occur that a specific event is logged before the event starts)

* Every line in the file is a JSON object containing event data:

	* id - the unique event identifier
	* state - whether the event was started or finished (can have values "STARTED" or "FINISHED"
	* timestamp - the timestamp of the event in milliseconds
	* Application Server logs also have the additional attributes:
		* type - type of log
		* host - hostname`


* Example:

	* {"id":"scsmbstgra", "state":"STARTED", "type":"APPLICATION_LOG", "host":"12345", "timestamp":1491377495212}`
	* {"id":"scsmbstgrb", "state":"STARTED", "timestamp":1491377495213}
	* {"id":"scsmbstgrc", "state":"FINISHED", "timestamp":1491377495218}
	* {"id":"scsmbstgra", "state":"FINISHED", "type":"APPLICATION_LOG", "host":"12345", "timestamp":1491377495217}
	* {"id":"scsmbstgrc", "state":"STARTED", "timestamp":1491377495210}
	* {"id":"scsmbstgrb", "state":"FINISHED", "timestamp":1491377495216}	


Project details
---------------

- Java 1.8
- Gradle build tool, 4.6
- Bitbucket repository
- Jacoco plugin
- Junit, Mockito

Project Repository
------------------
- Use the command below to clone the project from bitbucket to your local folder

		git clone https://preetham0411@bitbucket.org/preetham0411/cs-logeventreader.git
	
- Or Download the project using the link [cs-logeventreader](https://preetham0411@bitbucket.org/preetham0411/cs-logeventreader/downloads)

Build Project
-------------
- Go to the project root directory and run the command below to clean build the project

		gradle clean build


Run Test cases
--------------
- Go to the project root directory and run the command below to execute test cases

		gradle test
	
- Test reports can be found under ${project.dir}/build/reports/tests/

Generate Code Coverage
----------------------
- Go to the project root directory and run the command below to generate code coverage

		gradle jacoco
	
- Coverage reports can be found under ${project.dir}/build/reports/jacoco/

Static Code Analysis
--------------------
- Static code analysis is conducted on the fly using sonarlint plugin on eclipse.