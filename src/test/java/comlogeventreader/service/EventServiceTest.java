package comlogeventreader.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.logeventreader.EventReaderApplication;
import com.logeventreader.dao.Event;
import com.logeventreader.reopsitory.EventRepository;
import com.logeventreader.service.EventServiceImpl;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EventReaderApplication.class)
public class EventServiceTest extends TestCase {
	@Spy
	@InjectMocks
	EventServiceImpl eventService;
	
	@Mock
	EventRepository eventRepository;
	
	@Test
	public void findAll() {
		when(eventRepository.findAll()).thenReturn(getEvents());
		List<Event> events = eventService.findAll();
		assertEquals("Expected events is 3.", 3, events.size());
	}
	
	@Test
	public void save() {
		Event e = new Event();
		e.setId("scsmbstgra");
		e.setAlert(true);
		e.setType("APPLICATION_LOG");
		e.setDuration(5);
		e.setHost("12345");
		when(eventRepository.save(e)).thenReturn(e);
		Event event = eventService.save(e);
		assertEquals("Alarm is raised. ", true, event.isAlert());
		assertEquals("Event duration is 5 seconds ",5, event.getDuration());
	}
	
	private List<Event> getEvents() {
		List<Event> mockList = new ArrayList<>();
		for (int i = 0; i < 3; i++)
			mockList.add(new Event());
		return mockList;
	}
	
	
}
