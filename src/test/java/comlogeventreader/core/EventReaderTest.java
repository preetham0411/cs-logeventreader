package comlogeventreader.core;

import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.logeventreader.EventReaderApplication;
import com.logeventreader.core.EventReader;
import com.logeventreader.dao.Event;
import com.logeventreader.dto.EventDto;
import com.logeventreader.service.EventService;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EventReaderApplication.class)
public class EventReaderTest extends TestCase {
	
	@Spy
	@InjectMocks
	EventReader eventReader = new EventReader();
	
	@Mock
	EventService eventService;
	
	
	@Test
	public void generateEventsFromFile() throws IOException {
		URL url = this.getClass().getResource("/events.log");
		File file = new File(url.getFile());
		List<EventDto> eventDtos = eventReader.generateEventsFromFile(file.getAbsolutePath());
		assertEquals(6, eventDtos.size());
	}
	
	@Test
	public void mapDtoToEntity() {
		List<EventDto> mockList = getEventDtos();
		List<Event> events = eventReader.mapDtoToEntity(mockList);
		assertEquals("Event expected is 1. ", 1, events.size());
	}
	
	@Test
	public void saveAll() {
		when(eventService.save(new Event())).thenReturn(new Event());
		eventReader.saveAll(getEvents());
	}
	
	@Test
	public void findAll() {
		when(eventService.findAll()).thenReturn(getEvents());
		List<Event> events = eventReader.findAll();
		assertEquals(3, events.size());
	}

	private List<EventDto> getEventDtos() {
		List<EventDto> mockList = new ArrayList<>();
		EventDto e = new EventDto();
		e.setId("scsmbstgra");
		e.setState("STARTED");
		e.setType("APPLICATION_LOG");
		e.setTimestamp(1491377495217L);
		e.setHost("12345");
		mockList.add(e);
		e = new EventDto();
		e.setId("scsmbstgra");
		e.setState("FINISHED");
		e.setType("APPLICATION_LOG");
		e.setTimestamp(1491377495212L);
		e.setHost("12345");
		mockList.add(e);
		return mockList;
	}
	
	private List<Event> getEvents() {
		List<Event> mockList = new ArrayList<>();
		for (int i = 0; i < 3; i++)
			mockList.add(new Event());
		return mockList;
	}

}
