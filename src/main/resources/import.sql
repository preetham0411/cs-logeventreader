CREATE TABLE events (
   id VARCHAR(45)  NOT NULL,
   type VARCHAR (45),
   host VARCHAR (45),
   duration INTEGER,
   alert TINYINT,      
   PRIMARY KEY (ID)
); 