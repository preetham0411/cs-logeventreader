package com.logeventreader.core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logeventreader.EventReaderApplication;
import com.logeventreader.dao.Event;
import com.logeventreader.dto.EventDto;
import com.logeventreader.service.EventService;

@Component
public class EventReader {
	public static final Logger LOG = LoggerFactory.getLogger(EventReaderApplication.class);
	
	@Autowired
	@Qualifier("eventServiceImpl")
	private EventService eventService;
	
	public List<EventDto> generateEventsFromFile(final String filepath) throws IOException {
		final List<EventDto> eventDtos = new ArrayList<>();
		final ObjectMapper objectMapper = new ObjectMapper();
        try (@SuppressWarnings("resource")
		Stream<String> stream = new BufferedReader(new FileReader(filepath)).lines()) {
            eventDtos.addAll(0, stream.parallel().map(e -> {
            	EventDto edto = null;
				try {
					LOG.info("Event {}", e);
					edto = objectMapper.readValue(e, EventDto.class);
				} catch (IOException e1) {
					LOG.error("Could not read event from log file");
					LOG.error(e1.getMessage(), e1);
				}
				return edto;
			}).collect(Collectors.toList()));
        }
        LOG.info("Total Event Dtos generated from log file: {}", eventDtos.size());
        return eventDtos;
	}

	public List<Event> mapDtoToEntity(final List<EventDto> eventDtos) {
		LOG.info("Map DTO to Entity");
        final Map<String, Event> eventMap = new HashMap<>();
        List<Event> events = new ArrayList<>();
        eventDtos.forEach(e -> {
        	if(!eventMap.containsKey(e.getId())) {
        		Event event = new Event();
        		event.setHost(e.getHost());
        		event.setId(e.getId());
        		event.setDuration(e.getTimestamp());
        		eventMap.put(e.getId(), event);
        	} else {
        		Event event = eventMap.get(e.getId());
        		Long duration = Math.abs(e.getTimestamp() - event.getDuration());
        		event.setDuration(duration);
        		event.setAlert(duration > 4 ? Boolean.TRUE : Boolean.FALSE);
        		events.add(event);
        	}
        });
        return events;
	}
	
	public void saveAll(List<Event> events) {
		LOG.info("saving events to event table. Total events received: {}", events.size());
		events.parallelStream().forEach(e -> eventService.save(e));
	}
	public List<Event> findAll() {
		LOG.info("calling findAll ");
		return eventService.findAll();
	}
}
