package com.logeventreader.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logeventreader.dao.Event;
import com.logeventreader.reopsitory.EventRepository;

@Service
public class EventServiceImpl implements EventService  {
	private static final Logger LOG = LoggerFactory.getLogger(EventServiceImpl.class);
	@Autowired
	private EventRepository eventRepository;
	
	@Transactional
	public Event save(Event entity) {
		LOG.info("saving - event : {}, with duration > 4ms: {}, alert raised: {}" , entity.getId(), entity.getDuration(), entity.isAlert());
		return eventRepository.save(entity);
	}

	@Override
	public List<Event> findAll() {
		LOG.info("retrieve all events stored");
		return eventRepository.findAll();
	}
}
