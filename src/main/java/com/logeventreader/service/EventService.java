package com.logeventreader.service;

import java.util.List;

import com.logeventreader.dao.Event;

public interface EventService {
	public Event save(Event entity);
	public List<Event> findAll();
}
