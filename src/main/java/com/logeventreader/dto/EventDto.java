package com.logeventreader.dto;

public class EventDto {
	private String id;
	private String host;
	private String type;
	private Long timestamp;
	private String state;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "EventDto [id=" + id + ", host=" + host + ", type=" + type + ", timestamp=" + timestamp + ", state="
				+ state + "]";
	}	
	
}
