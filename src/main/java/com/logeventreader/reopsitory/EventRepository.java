package com.logeventreader.reopsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logeventreader.dao.Event;

public interface EventRepository extends JpaRepository<Event, String> {

}
