package com.logeventreader;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.logeventreader.core.EventReader;
import com.logeventreader.dao.Event;
import com.logeventreader.dto.EventDto;

@SpringBootApplication
public class EventReaderApplication implements CommandLineRunner {
	
	public static final Logger LOG = LoggerFactory.getLogger(EventReaderApplication.class);
	
	@Autowired
	EventReader eventReader;
	
	public static void main(String...args) {
		LOG.info("Starting application");
		SpringApplication app = new SpringApplication(EventReaderApplication.class);
		app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
	}
	
	

    @Override
    public void run(String... arg0) throws Exception {
    	LOG.info("Reading log file to find alert for duration > 4");
    	List<EventDto> eventDtos = eventReader.generateEventsFromFile("C:\\events.log");
    	List<Event> events = eventReader.mapDtoToEntity(eventDtos);
    	eventReader.saveAll(events);
    	LOG.info("Retrieving events from database");
    	eventReader.findAll().forEach(e -> LOG.info(e.getId()));
    }
}
